# Explorables

Collection of (definitely) random and (hopefully) interesting stuff written mostly in Rust. 

##### A* Visualized
An A* search algorithm visualization and comparison with Dijsktra's algorithm in 2d pathfinding.

##### Floyd-Steinberg Dithering
An implementation of Floyd-Steinberg dithering in Processing as explored in [this video by The Coding Train]. Also some experiments with drawing the compressed image with white circles on a black background. Plans to port it to Rust do exist.

##### Pi Bouncing Boxes
Calculation of the first *n* digits of *Pi* by simulating bouncing boxes. The concept is explainted in [this video by 3Blue1Brown].


[this video by The Coding Train]: <https://youtu.be/0L2n8Tg2FwI>
[this video by 3Blue1Brown]: <https://youtu.be/HEfHFsfGXjs>