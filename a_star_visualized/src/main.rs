use ggez;
use noise::{NoiseFn, Perlin};
use priority_queue::PriorityQueue;
use rand::Rng;
use std::{
    collections::{HashMap, HashSet},
    env, path,
};

const SHOW: bool = true;
const VERBOSE: bool = false;

const SIZE: i32 = 40;
const WID: i32 = 24;
const HIG: i32 = 18;
const UI_BAR: f32 = 24.0;

const PERLIN_SCALE: f64 = 0.33;
const OBSTACLE_THRESHOLD: f64 = 0.25;

fn main() -> ggez::GameResult {
    let mut cb = ggez::ContextBuilder::new("A* pathfinding visualized", "Maciej Wojno")
        .window_setup(
            ggez::conf::WindowSetup::default()
                .title("A* pathfinding visualized")
                .vsync(true),
        )
        .window_mode(
            ggez::conf::WindowMode::default()
                .dimensions(SIZE as f32 * WID as f32, SIZE as f32 * HIG as f32 + UI_BAR),
        );

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        cb = cb.add_resource_path(path);
    }

    let (mut ctx, mut events_loop) = cb.build()?;

    let mut state = GameState::new(&mut ctx);
    ggez::event::run(&mut ctx, &mut events_loop, &mut state)
}

struct GameState {
    pathfinder: AStarPathfinder,
    obstacles: Vec<i64>,
    start: i64,
    goal: i64,
    font: Option<ggez::graphics::Font>,
    auto_update: bool,
    use_a_star: bool,
    show_costs: bool,
    slow_down: bool,
}

impl GameState {
    pub fn new(ctx: &mut ggez::Context) -> Self {
        let (start, goal, obstacles) = GameState::generate_map();
        let pathfinder = AStarPathfinder::new((WID, HIG), start, goal, obstacles.as_slice())
            .expect("Pathfinder generation error");
        GameState {
            pathfinder,
            obstacles,
            start,
            goal,
            font: match ggez::graphics::Font::new(ctx, "/FreeMonoBold.ttf") {
                Ok(font) => Some(font),
                Err(_) => {
                    eprintln!("Error loading font!");
                    None
                }
            },
            auto_update: false,
            use_a_star: true,
            show_costs: true,
            slow_down: true,
        }
    }

    pub fn reset(&mut self) {
        self.pathfinder =
            AStarPathfinder::new((WID, HIG), self.start, self.goal, self.obstacles.as_slice())
                .expect("Pathfinder generation error");
    }

    pub fn next(&mut self) {
        let (start, goal, obstacles) = GameState::generate_map();
        self.start = start;
        self.goal = goal;
        self.obstacles = obstacles;
        self.reset();
    }

    fn generate_map() -> (i64, i64, Vec<i64>) {
        let noise = Perlin::new();
        let mut obstacles: Vec<i64> = Vec::new();
        for x in 0..WID {
            for y in 0..HIG {
                if noise.get([x as f64 * PERLIN_SCALE + 0.1, y as f64 * PERLIN_SCALE + 0.1])
                    > OBSTACLE_THRESHOLD
                {
                    obstacles.push(x as i64 + y as i64 * WID as i64);
                };
            }
        }
        let mut rng = rand::thread_rng();
        let mut start: i64;
        loop {
            start = rng.gen_range(0, WID * HIG) as i64;
            if !obstacles.contains(&start) {
                break;
            };
        }
        let mut goal: i64;
        loop {
            goal = rng.gen_range(0, WID * HIG) as i64;
            if !obstacles.contains(&goal) {
                break;
            };
        }
        if VERBOSE {
            println!(
                "{} {}\n{} {}",
                start / WID as i64,
                start % WID as i64,
                goal / WID as i64,
                goal % WID as i64,
            );
        };
        return (start, goal, obstacles);
    }

    fn draw_settings(&self, ctx: &mut ggez::Context) -> ggez::GameResult {
        fn draw_setting(
            ctx: &mut ggez::Context,
            setting: &str,
            value: Option<bool>,
            font: Option<ggez::graphics::Font>,
            x: f32,
            y: f32,
        ) -> Result<f32, ggez::GameError> {
            let mut text = ggez::graphics::Text::new(setting);
            if let Some(font) = font {
                text.set_font(font, ggez::graphics::Scale { x: 1.0, y: 1.0 });
            }
            let position = ggez::mint::Point2 {
                x: x + 30.0,
                y: y + UI_BAR / 2.0 - text.height(ctx) as f32 / 2.0,
            };
            let color = match value {
                None => ggez::graphics::BLACK,
                Some(true) => [0.0, 0.75, 0.0, 1.0].into(),
                Some(false) => [0.75, 0.0, 0.0, 1.0].into(),
            };
            ggez::graphics::draw(ctx, &text, (position, color))?;
            Ok(position.x + text.width(ctx) as f32)
        }
        let settings = [
            ("|Q|Quit", None),
            ("|R|Reset", None),
            ("|N|Next", None),
            ("|Space|Step", None),
            ("|A|Auto-update", Some(self.auto_update)),
            ("|S|Slowdown", Some(self.slow_down)),
            ("|U|Use A*", Some(self.use_a_star)),
            ("|C|Show costs", Some(self.show_costs)),
        ];
        let mut posx = 0.0;
        for setting in settings.iter() {
            posx = draw_setting(
                ctx,
                setting.0,
                setting.1,
                self.font,
                posx,
                (HIG * SIZE) as f32,
            )?;
        }
        Ok(())
    }
}

enum AStarState {
    Looking,
    Found,
    Failed,
}

struct AStarPathfinder {
    state: AStarState,
    path: Vec<i64>,
    width: i32,
    height: i32,
    start: i64,
    goal: i64,
    obstacles: HashSet<i64>,
    gscore: HashMap<i64, i32>,
    fscore: HashMap<i64, i32>,
    openset: PriorityQueue<i64, i32>,
    camefrom: HashMap<i64, i64>,
    obstacle_mesh: Option<ggez::graphics::Mesh>,
}

impl AStarPathfinder {
    pub fn new((w, h): (i32, i32), start: i64, goal: i64, obs: &[i64]) -> Result<Self, ()> {
        if start >= w as i64 * h as i64 || goal >= w as i64 * h as i64 {
            return Err(());
        }
        let mut obstacles = HashSet::new();
        for o in obs.iter() {
            if *o >= w as i64 * h as i64 {
                return Err(());
            } else {
                obstacles.insert(*o);
            };
        }
        let mut openset = PriorityQueue::new();
        openset.push(
            start,
            std::i32::MAX - AStarPathfinder::estimate_cost(w, start, goal),
        );
        let camefrom = HashMap::new();
        let mut gscore = HashMap::new();
        gscore.insert(start, 0);
        let mut fscore = HashMap::new();
        fscore.insert(start, AStarPathfinder::estimate_cost(w, start, goal));

        Ok(AStarPathfinder {
            state: AStarState::Looking,
            path: Vec::new(),
            width: w,
            height: h,
            start,
            goal,
            obstacles,
            gscore,
            fscore,
            openset,
            camefrom,

            obstacle_mesh: None,
        })
    }

    pub fn step(&mut self, use_a_star: bool) {
        if let AStarState::Looking = self.state {
            let current = self.openset.pop();
            match current {
                Some(current) => {
                    // reached goal
                    if current.0 == self.goal {
                        match self.reconstruct_path() {
                            Ok(()) => {
                                self.state = AStarState::Found;
                                return;
                            }
                            Err(()) => {
                                panic!("Failed to reconstruct path!");
                            }
                        }
                    // goal not reached
                    } else {
                        let neighbors = self.neighbors(current.0);
                        for (neighbor, d) in neighbors.iter() {
                            let tcost = self.gscore.get(&current.0).unwrap() + d;
                            if !self.gscore.contains_key(neighbor)
                                || self.gscore.get(neighbor).unwrap() > &tcost
                            {
                                self.camefrom.insert(*neighbor, current.0);
                                self.gscore.insert(*neighbor, tcost);
                                let fscore = if use_a_star {
                                    tcost
                                        + AStarPathfinder::estimate_cost(
                                            self.width, *neighbor, self.goal,
                                        )
                                } else {
                                    tcost
                                };
                                self.fscore.insert(*neighbor, fscore);
                                self.openset
                                    .push_increase(*neighbor, std::i32::MAX - fscore);
                            }
                        }
                    }
                }
                // openset is empty and goal wasn't reached
                None => {
                    self.state = AStarState::Failed;
                    return;
                }
            }
        }
    }

    fn reconstruct_path(&mut self) -> Result<(), ()> {
        self.path.clear();
        self.path.push(self.goal);
        let mut current = self.goal;
        loop {
            let next = self.camefrom.get(&current);
            match next {
                Some(x) => {
                    current = *x;
                    self.path.push(current);
                    if current == self.start {
                        return Ok(());
                    }
                }
                None => {
                    return Err(());
                }
            };
        }
    }

    fn neighbors(&self, current: i64) -> Vec<(i64, i32)> {
        let mut nb = Vec::new();
        if current / self.width as i64 != 0
            && !self.obstacles.contains(&(current - self.width as i64))
        {
            nb.push((current - self.width as i64, 10));
        };
        if current / self.width as i64 != 0
            && current % self.width as i64 != self.width as i64 - 1
            && !self.obstacles.contains(&(current - self.width as i64 + 1))
        {
            nb.push((current - self.width as i64 + 1, 14));
        };
        if current % self.width as i64 != self.width as i64 - 1
            && !self.obstacles.contains(&(current + 1))
        {
            nb.push((current + 1, 10));
        };
        if current / self.width as i64 != self.height as i64 - 1
            && current % self.width as i64 != self.width as i64 - 1
            && !self.obstacles.contains(&(current + self.width as i64 + 1))
        {
            nb.push((current + self.width as i64 + 1, 14));
        };
        if current / self.width as i64 != self.height as i64 - 1
            && !self.obstacles.contains(&(current + self.width as i64))
        {
            nb.push((current + self.width as i64, 10));
        };
        if current / self.width as i64 != self.height as i64 - 1
            && current % self.width as i64 != 0
            && !self.obstacles.contains(&(current + self.width as i64 - 1))
        {
            nb.push((current + self.width as i64 - 1, 14));
        };
        if current % self.width as i64 != 0 && !self.obstacles.contains(&(current - 1)) {
            nb.push((current - 1, 10));
        };
        if current / self.width as i64 != 0
            && current % self.width as i64 != 0
            && !self.obstacles.contains(&(current - self.width as i64 - 1))
        {
            nb.push((current - self.width as i64 - 1, 14));
        };
        return nb;
    }

    pub fn path(&self) -> Option<&Vec<i64>> {
        match self.state {
            AStarState::Found => Some(&self.path),
            _ => None,
        }
    }

    pub fn estimate_cost(w: i32, ps: i64, pg: i64) -> i32 {
        let dx = (ps / w as i64 - pg / w as i64).abs();
        let dy = (ps % w as i64 - pg % w as i64).abs();
        let diagonal = i64::min(dx, dy);
        let straight = i64::max(dx, dy) - diagonal;
        (diagonal * 14 + straight * 10) as i32
    }

    pub fn update(&mut self, use_a_star: bool) {
        if let AStarState::Looking = self.state {
            self.step(use_a_star);
            match self.state {
                AStarState::Looking => {
                    if VERBOSE {
                        print!(".");
                    };
                }
                AStarState::Found => {
                    if VERBOSE {
                        println!("\nFound path!");
                    };
                }
                AStarState::Failed => {
                    if VERBOSE {
                        println!("\nFailed to find path!");
                    };
                }
            };
        };
    }

    pub fn draw(&mut self, ctx: &mut ggez::Context, show_costs: bool) -> ggez::GameResult {
        let bg = ggez::graphics::Mesh::new_rectangle(
            ctx,
            ggez::graphics::DrawMode::fill(),
            ggez::graphics::Rect::new_i32(0, 0, self.width * SIZE, self.height * SIZE),
            ggez::graphics::Color::new(0.75, 0.75, 0.75, 1.0),
        )?;
        ggez::graphics::draw(ctx, &bg, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;

        if let None = self.obstacle_mesh {
            if !self.obstacles.is_empty() {
                let mut obstacles_mesh_builder = ggez::graphics::MeshBuilder::new();
                for o in self.obstacles.iter() {
                    let ox = o % self.width as i64;
                    let oy = o / self.width as i64;
                    obstacles_mesh_builder.rectangle(
                        ggez::graphics::DrawMode::fill(),
                        ggez::graphics::Rect::new_i32(
                            ox as i32 * SIZE,
                            oy as i32 * SIZE,
                            SIZE,
                            SIZE,
                        ),
                        ggez::graphics::BLACK,
                    );
                }
                self.obstacle_mesh = Some(obstacles_mesh_builder.build(ctx)?);
            }
        }
        if let Some(mesh) = &self.obstacle_mesh {
            ggez::graphics::draw(ctx, mesh, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        }

        if !self.fscore.is_empty() {
            let mut closed_mesh_builder = ggez::graphics::MeshBuilder::new();
            for (f, _) in self.fscore.iter() {
                let fx = f % self.width as i64;
                let fy = f / self.width as i64;
                closed_mesh_builder.rectangle(
                    ggez::graphics::DrawMode::fill(),
                    ggez::graphics::Rect::new_i32(fx as i32 * SIZE, fy as i32 * SIZE, SIZE, SIZE),
                    ggez::graphics::Color::new(0.5, 0.25, 0.25, 1.0),
                );
            }
            let closed_mesh = closed_mesh_builder.build(ctx)?;
            ggez::graphics::draw(ctx, &closed_mesh, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        }

        if !self.openset.is_empty() {
            let mut open_mesh_builder = ggez::graphics::MeshBuilder::new();
            for (f, _) in self.openset.iter() {
                let fx = f % self.width as i64;
                let fy = f / self.width as i64;
                open_mesh_builder.rectangle(
                    ggez::graphics::DrawMode::fill(),
                    ggez::graphics::Rect::new_i32(fx as i32 * SIZE, fy as i32 * SIZE, SIZE, SIZE),
                    ggez::graphics::Color::new(0.25, 0.5, 0.25, 1.0),
                );
            }
            let open_mesh = open_mesh_builder.build(ctx)?;
            ggez::graphics::draw(ctx, &open_mesh, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        };

        if let Some(path) = self.path() {
            for p in path.iter() {
                let px = p % self.width as i64;
                let py = p / self.width as i64;
                let rect = ggez::graphics::Mesh::new_rectangle(
                    ctx,
                    ggez::graphics::DrawMode::fill(),
                    ggez::graphics::Rect::new_i32(px as i32 * SIZE, py as i32 * SIZE, SIZE, SIZE),
                    ggez::graphics::Color::new(0.5, 0.5, 0.5, 1.0),
                )?;
                ggez::graphics::draw(ctx, &rect, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
            }
        }

        {
            let sx = self.start % self.width as i64;
            let sy = self.start / self.width as i64;
            let rect = ggez::graphics::Mesh::new_rectangle(
                ctx,
                ggez::graphics::DrawMode::fill(),
                ggez::graphics::Rect::new_i32(sx as i32 * SIZE, sy as i32 * SIZE, SIZE, SIZE),
                ggez::graphics::Color::new(0.0, 0.0, 1.0, 1.0),
            )?;
            ggez::graphics::draw(ctx, &rect, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        }

        {
            let gx = self.goal % self.width as i64;
            let gy = self.goal / self.width as i64;
            let rect = ggez::graphics::Mesh::new_rectangle(
                ctx,
                ggez::graphics::DrawMode::fill(),
                ggez::graphics::Rect::new_i32(gx as i32 * SIZE, gy as i32 * SIZE, SIZE, SIZE),
                ggez::graphics::Color::new(0.5, 0.5, 0.0, 1.0),
            )?;
            ggez::graphics::draw(ctx, &rect, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        }

        if show_costs {
            for f in self.gscore.iter() {
                let fx = f.0 % self.width as i64 * SIZE as i64;
                let fy = f.0 / self.width as i64 * SIZE as i64;
                let text = ggez::graphics::Text::new(format!("{}", f.1));
                let position = ggez::mint::Point2 {
                    x: fx as f32 + (SIZE as f32 - text.width(ctx) as f32) / 2.0,
                    y: fy as f32 + (SIZE as f32 / 2.0) - text.height(ctx) as f32,
                };
                let color = ggez::graphics::WHITE;
                ggez::graphics::queue_text(ctx, &text, position, Some(color));
            }
            for f in self.fscore.iter() {
                let fx = f.0 % self.width as i64 * SIZE as i64;
                let fy = f.0 / self.width as i64 * SIZE as i64;
                let text = ggez::graphics::Text::new(format!("{}", f.1));
                let position = ggez::mint::Point2 {
                    x: fx as f32 + (SIZE as f32 - text.width(ctx) as f32) / 2.0,
                    y: fy as f32 + SIZE as f32 - text.height(ctx) as f32,
                };
                let color = ggez::graphics::WHITE;
                ggez::graphics::queue_text(ctx, &text, position, Some(color));
            }
            ggez::graphics::draw_queued_text(ctx, (ggez::mint::Point2{x:0.0,y:0.0},), None, ggez::graphics::FilterMode::Linear)?;
        }

        Ok(())
    }
}

impl ggez::event::EventHandler for GameState {
    fn update(&mut self, ctx: &mut ggez::Context) -> ggez::GameResult {
        if self.auto_update {
            let time_start = ggez::timer::time_since_start(ctx).as_secs_f64();
            loop {
                self.pathfinder.update(self.use_a_star);
                if self.slow_down
                    || ggez::timer::time_since_start(ctx).as_secs_f64() - time_start >= 0.01
                {
                    break;
                }
            }
        };
        Ok(())
    }

    fn draw(&mut self, ctx: &mut ggez::Context) -> ggez::GameResult {
        if SHOW {
            ggez::graphics::clear(ctx, ggez::graphics::WHITE);

            self.pathfinder.draw(ctx, self.show_costs)?;
            self.draw_settings(ctx)?;

            ggez::graphics::present(ctx)?;
            ggez::timer::yield_now();
        }
        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut ggez::Context,
        keycode: ggez::event::KeyCode,
        _keymod: ggez::event::KeyMods,
        repeat: bool,
    ) {
        use ggez::input::keyboard::KeyCode;
        if !repeat {
            match keycode {
                KeyCode::Space if !self.auto_update => self.pathfinder.update(self.use_a_star),
                KeyCode::A => self.auto_update = !self.auto_update,
                KeyCode::U => self.use_a_star = !self.use_a_star,
                KeyCode::S => self.slow_down = !self.slow_down,
                KeyCode::C => self.show_costs = !self.show_costs,
                KeyCode::R => self.reset(),
                KeyCode::N => self.next(),
                KeyCode::Q => ggez::event::quit(ctx),
                _ => (),
            }
        }
    }
}
