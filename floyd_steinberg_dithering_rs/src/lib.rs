use std::thread;

use image::{DynamicImage, Rgba, RgbaImage};

pub fn dither_image(src: &DynamicImage) -> RgbaImage {
    let src = src.to_rgba8();
    let w = src.width();
    let h = src.height();

    let mut image = RgbaImage::from_raw(w, h, vec![0; (w as usize) * (h as usize) * 4]).unwrap();

    for x in 0..w {
        for y in 0..h {
            dither_pixel(x, y, &src, &mut image);
        }
    }

    image
}

fn dither_pixel(x: u32, y: u32, src: &RgbaImage, image: &mut RgbaImage) {

}

fn quantize(value: u8, levels: u8) -> u8 {
    
}