use ggez;

const DIGITS: i8 = 3;

fn main() -> ggez::GameResult {
    let (ctx, events_loop) = &mut ggez::ContextBuilder::new("Calculate pi digits by bouncing boxes", "Maciej Wojno")
        .window_setup(ggez::conf::WindowSetup::default().title("Calculate pi digits by bouncing boxes"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(1280.0, 720.0))
        .build()?;

    let state = &mut Boxes::with_pi_digits(DIGITS);
    ggez::event::run(ctx, events_loop, state)
}

struct Box {
    pub position: (f64, f64),
    pub size: f64,
    pub mass: f64,
    pub velocity: f64,
}

impl Box {
    pub fn new(x: f64, size: f64, mass: f64, velocity: f64) -> Self {
        Box {
            position: (x, 360.0),
            size,
            mass,
            velocity,
        }
    }

    pub fn draw(&self, ctx: &mut ggez::Context) -> ggez::GameResult {
        let rect = ggez::graphics::Mesh::new_rectangle(
            ctx,
            ggez::graphics::DrawMode::fill(),
            ggez::graphics::Rect::new(
                self.position.0 as f32,
                (self.position.1 - self.size) as f32,
                self.size as f32,
                self.size as f32,
            ),
            ggez::graphics::WHITE,    
        )?;
        ggez::graphics::draw(ctx, &rect, (ggez::mint::Point2 {x: 0.0, y: 0.0}, ))?;
        Ok(())
    }

    fn velocity_after_bounce(&self, other: &Box) -> f64 {
        ((self.mass - other.mass) * self.velocity / (self.mass + other.mass)) 
        + (2.0 * other.mass * other.velocity / (self.mass + other.mass))
    }
}

#[derive(Copy, Clone)]
enum Bounce {
    Wall,
    Boxes,
}

pub struct Boxes {
    left: Box,
    right: Box,
    digits: i8,
    bounces: i64,
}

impl Boxes {
    pub fn with_pi_digits(digits: i8) -> Self {
        let dig_checked = if digits > 0 { digits } else { 1 };
        Boxes {
            left: Box::new(500.0, 50.0, 1.0, 0.0),
            right: Box::new(800.0, 100.0, 100f64.powi(dig_checked as i32 - 1), -500.0 / dig_checked as f64),
            digits: dig_checked,
            bounces: 0,
        }
    }

    fn bounce_boxes(&mut self) {
        self.bounces += 1;
        let lv = self.left.velocity_after_bounce(&self.right);
        self.right.velocity = self.right.velocity_after_bounce(&self.left);
        self.left.velocity = lv;
    }

    fn bounce_wall(&mut self) {
        self.bounces += 1;
        self.left.velocity *= -1.0;
    }

    fn next_bounce(&self) -> (Bounce, f64) {
        let time_to_wall = if self.left.velocity < 0.0 {
            - self.left.position.0 / self.left.velocity
        } else {
            std::f64::MAX
        };
        let time_to_boxes = if self.left.velocity - self.right.velocity > 0.0 {
            (self.right.position.0 - self.left.position.0 - self.left.size) / (self.left.velocity - self.right.velocity)
        } else {
            std::f64::MAX
        };
        if time_to_wall < time_to_boxes {
            (Bounce::Wall, time_to_wall)
        } else {
            (Bounce::Boxes, time_to_boxes)
        }
    }

    fn move_boxes(&mut self, time: f64) {
        self.left.position.0 += self.left.velocity * time;
        self.right.position.0 += self.right.velocity * time;
    }

    fn print_bounces(&self, ctx: &mut ggez::Context) -> ggez::GameResult {
        let text = ggez::graphics::Text::new(format!("{:01$}", self.bounces, self.digits as usize));
        ggez::graphics::draw(ctx, &text, (ggez::mint::Point2 {x: 100.0, y: 100.0}, ))?;
        Ok(())
    }
}

impl ggez::event::EventHandler for Boxes {
        fn update(&mut self, ctx: &mut ggez::Context) -> ggez::GameResult {
            let dt = ggez::timer::delta(ctx).as_secs_f64();

            let mut time_left = dt;
            let mut next_bounce = self.next_bounce();
            let mut time_passed = 0.0;
            while next_bounce.1 < time_left && time_passed < 1.0 / 60.0 {
                time_left -= next_bounce.1;
                time_passed += next_bounce.1;
                self.move_boxes(next_bounce.1);
                match next_bounce.0 {
                    Bounce::Wall => { self.bounce_wall() },
                    Bounce::Boxes => { self.bounce_boxes() },
                }
                next_bounce = self.next_bounce();
            }
            if next_bounce.1 < time_left {
                self.move_boxes(next_bounce.1 / 2.0)
            } else {
                self.move_boxes(time_left)
            }

            Ok(())
        }
    
        fn draw(&mut self, ctx: &mut ggez::Context) -> ggez::GameResult {
            ggez::graphics::clear(ctx, ggez::graphics::BLACK);

            self.left.draw(ctx)?;
            self.right.draw(ctx)?;
            self.print_bounces(ctx)?;

            ggez::graphics::present(ctx)?;
            ggez::timer::yield_now();
            Ok(())
        }
    
        fn key_down_event(
            &mut self,
            _ctx: &mut ggez::Context,
            _keycode: ggez::event::KeyCode,
            _keymod: ggez::event::KeyMods,
            _repeat: bool,
        ) {
        }
}
