PImage image;
PImage dithered;
int COLORS = 4;
int MAGNIFICATION = 2;

void setup() {
  size(1500, 750);
  background(0,0,0,0);
  image = loadImage("kitten.jpg");
  dithered = image.copy();
  image(image, 0, 0);
  dithered.filter(GRAY);
  dither_image(dithered);
  image(dithered, 750, 0);
  dithered.save("data/dithered.png");
  //drawWithCircles(dithered, 750, 0, 3);
}

void drawWithCircles(PImage img, int px, int py, float scale) {
  for (int x = 0; x < img.width; x += scale) {
    for (int y = 0;  y < img.height; y += scale) {
      float size = 0;
      for (int i = 0; i < scale && x + i < img.width; ++i) {
        for (int u = 0; u < scale && y + u < img.height; ++u) {
          color c = img.pixels[index(img, x + i, y + u)];
          size = ( red(c) + green(c) + blue(c) ) / 765.0 * scale;
        }
      }
      fill(255,255,255,255);
      stroke(255,255,255,255);
      circle(
        px + x * MAGNIFICATION + scale * MAGNIFICATION * 0.5,
        py + y * MAGNIFICATION + scale * MAGNIFICATION * 0.5,
        size * MAGNIFICATION * 1.5
      );
    }
  }
  
}

int index(PImage img, int x, int y) {
  return x + y * img.width;
}

int quantize(float channel, int colors) {
  return round((colors - 1) * channel / 255.0) * 255/(colors - 1);
}

void dither_image(PImage img) {
  for (int y = 0; y < img.height; ++y) {
    for (int x = 0; x < img.width; ++x) {
      color pixel = img.pixels[index(img, x, y)];
      int r = quantize(red  (pixel), COLORS);
      int g = quantize(green(pixel), COLORS);
      int b = quantize(blue (pixel), COLORS);
      float errR = red  (pixel) - r;
      float errG = green(pixel) - g;
      float errB = blue (pixel) - b;
      img.pixels[index(img, x, y)] = color(r,g,b);
      if (x < img.width - 1)   push_error(img, index(img, x + 1, y    ), errR, errG, errB, 7/16.0);
      if (y < img.height - 1) {
        if (x > 0)             push_error(img, index(img, x - 1, y + 1), errR, errG, errB, 3/16.0);
                               push_error(img, index(img, x    , y + 1), errR, errG, errB, 5/16.0);
        if (x < img.width - 1) push_error(img, index(img, x + 1, y + 1), errR, errG, errB, 1/16.0);
      }
    }
  }
}

void push_error(PImage img, int id, float errR, float errG, float errB, float scale) {
  color pixel = img.pixels[id];
  float r = red  (pixel) + errR * scale;
  float g = green(pixel) + errG * scale;
  float b = blue (pixel) + errB * scale;
  img.pixels[id] = color(r,g,b);
}
